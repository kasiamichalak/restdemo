package pl.casmic.restdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.casmic.restdemo.model.Message;

@RestController
public class MessageController {

    @GetMapping("/message")
    Message viewMessage() {
        return new Message("first message");
    }

    @PostMapping("/message")
    Message echoMessage(@RequestBody Message message) {
        return message;
    }
}
